package edu.uweo.javaintro.homework.operators;

public class OperatorAdd extends OperatorBinary {

	public OperatorAdd( Operand operandLeft, Operand operandRight ) {
		super(PRIORITY_ADD, operandLeft, operandRight, OPERATOR_ADD);
	}

	@Override
	public double evaluate() {
		return getLeftValue() + getRightValue();
	}

}
