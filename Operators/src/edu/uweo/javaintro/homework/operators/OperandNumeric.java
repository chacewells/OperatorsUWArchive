package edu.uweo.javaintro.homework.operators;


public class OperandNumeric extends Operand {

	public OperandNumeric( String operand ) {
		super( operand );
	}
	
	public double evaluate() {
		double result = 0.0;
		try {
			if ( isOctal() )
				result = parseIntOctal();
			
			else if ( isHexadecimal() )
				result = parseIntHex();
			
			else if ( isBinary() )
				result = parseIntBinary();
			
			else
				result = parseDoubleDecimal();
			
		} catch (NumberFormatException e) {
			throw new NumberFormatException(
					String.format("%s is not a valid operand", getOperand() )
					);
		}
		
		return result;
	}
	
	private boolean isOctal() {
		return getOperand().matches("0[0-8]+");
	}
	
	private boolean isHexadecimal() {
		return getOperand().matches("0[Xx][A-Fa-f\\d]+"); 
	}
	
	private boolean isBinary() {
		return getOperand().matches("0[Bb][01]+");
	}
	
//	check whether this operand is a decimal number
//	but this object attempts to parse decimal by default, so it is not used
//	private boolean isDecimal() {
//		return getOperand().matches("\\d+");
//	}
	
	private double parseDoubleDecimal() {
		double result = Double.parseDouble( toString() );
		if ( toString().matches("-0\\.*0*") )
			result = Math.abs( result );
		
		return result;
	}
	
	private int parseIntHex() {
		return Integer.parseInt( toString().replaceAll("0[Xx]", ""), 16 );
	}
	
	private int parseIntBinary() {
		return Integer.parseInt( toString().replaceAll("0[Bb]", ""), 2 );
	}
	
	private int parseIntOctal() {
		return Integer.parseInt( toString(), 8 );
	}

}
