package edu.uweo.javaintro.homework.operators;

public class OperatorSubtract extends OperatorBinary {

	public OperatorSubtract( Operand operandLeft, Operand operandRight ) {
		super(PRIORITY_ADD, operandLeft, operandRight, OPERATOR_SUBTRACT);
	}
	
	@Override
	public double evaluate() {
		return getLeftValue() - getRightValue();
	}

}
