package edu.uweo.javaintro.homework.operators;

public class OperatorMultiply extends OperatorBinary {
	
	public OperatorMultiply( Operand operandLeft, Operand operandRight ) {
		super(PRIORITY_ADD, operandLeft, operandRight, OPERATOR_MULTIPLY);
	}

	@Override
	public double evaluate() {
		return getLeftValue() * getRightValue();
	}

}
