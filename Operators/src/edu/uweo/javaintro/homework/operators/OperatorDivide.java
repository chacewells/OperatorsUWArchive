package edu.uweo.javaintro.homework.operators;

public class OperatorDivide extends OperatorBinary {

	public OperatorDivide( Operand operandLeft, Operand operandRight ) {
		super(PRIORITY_ADD, operandLeft, operandRight, OPERATOR_DIVIDE);
	}

	@Override
	public double evaluate() {
		if ( getRightValue() == 0 )
			throw new IllegalArgumentException( "cannot divide by zero!");
		
		return getLeftValue() / getRightValue();
	}

}
