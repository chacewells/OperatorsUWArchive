package edu.uweo.javaintro.homework.operators;

public abstract class OperatorBinary extends Operator {
	private Operand	leftOperand, 
					rightOperand;
	private String operator;
	
	public OperatorBinary( int priority,
			Operand leftOperand, 
			Operand rightOperand, 
			String operator ) {
		
		super( priority );
		
		setLeftOperand( leftOperand );
		setRightOperand( rightOperand );
		setOperator( operator );
		
	}
	
	private void setLeftOperand( Operand leftOperand ) {
		if ( leftOperand == null )
			throw new NullPointerException("cannot initialize "
					+ "with null left operand");
		this.leftOperand = leftOperand;
	}
	
	private void setRightOperand(Operand rightOperand) {
		if ( rightOperand == null)
			throw new NullPointerException("cannot initialize "
					+ "with null right operand");
		this.rightOperand = rightOperand;
	}
	
	private void setOperator( String operator ) {
		if ( operator == null )
			throw new NullPointerException("cannot ititialize "
					+ "with null operator");
		this.operator = operator;
	}
	
	public double getLeftValue() {
		return leftOperand.evaluate();
	}
	
	public double getRightValue() {
		return rightOperand.evaluate();
	}
	
	@Override
	public String toString() {
		return (new StringBuilder())
				.append(leftOperand.toString())
				.append(operator)
				.append(rightOperand.toString())
				.toString();
	}
	
}
