package edu.uweo.javaintro.homework.operators;

public abstract class Operator {
	private int priority;
	
	protected static final String	OPERATOR_ADD		=	"+",
									OPERATOR_SUBTRACT	=	"-",
									OPERATOR_MULTIPLY	=	"*",
									OPERATOR_DIVIDE		=	"/";
	
	protected static final int	PRIORITY_ADD		=	4,
								PRIORITY_SUBTRACT	=	4,
								PRIORITY_MULTIPLY	=	5,
								PRIORITY_DIVIDE		=	5;
	
	public Operator( int priority ) {
		this.priority = priority;
	}
	
	public abstract double evaluate();
	
	public int getPriority() {
		return priority;
	}

}
