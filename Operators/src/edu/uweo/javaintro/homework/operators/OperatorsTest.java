package edu.uweo.javaintro.homework.operators;

import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;

public class OperatorsTest {
	
	private static final int PAD = 10;
	private static final String[] TEST_OPERANDS = {
//		binary positive
//		test new OperandNumeric( "0b1101" )
				"0b1101",
		
//		binary negative
//		test new OperandNumeric( "-0b1101" )
				"-0b1101",
		
//      binary positive zero
//		test new OperandNumeric( "0b0" )
				"0b0",
		
//      binary negative zero
//		test new OperandNumeric( "-0b0" )
				"-0b0",
		
//		octal positive
//		test new OperandNumeric( "017" )
				"017",
		
//		octal negative
//		test new OperandNumeric( "-017" )
				"-017",
		
//      octal positive zero
//		test new OperandNumeric( "00" )
				"00",
		
//      octal negative zero
//		test new OperandNumeric( "-00" )
				"-00",
		
//		base10 positive
//		test new OperandNumeric( "29" )
				"29",
		
//		base10 negative
//		test new OperandNumeric( "-29" )
				"-29",
		
//      base10 positive zero
//		test new OperandNumeric( "0" )
				"0",
		
//      base10 negative zero
//		test new OperandNumeric( "-0" )
				"-0",
		
//		hexadecimal positive
//		test new OperandNumeric( "0x2F" )
				"0x2e",
		
//		hexadecimal negative
//		test new OperandNumeric( "-0x2F" )
				"-0x2F",

//      hexadecimal positive zero
//		test new OperandNumeric( "0x0" )
				"0x0",
		
//      hexadecimal negative zero
//		test new OperandNumeric( "-0x0" )
				"-0x0",
		};
	
	public static void main(String[] args) {
		operandTest();
		System.out.println();
		
		for (int i=0; i++<60; System.out.print('*'));
		System.out.println();
		System.out.println();
		
		operatorTest();
		
	}
	
	private static void parseDemo() {
		String test = "3+5";
		StringTokenizer st = new StringTokenizer( test, "+-*/", true );
		
		while (st.hasMoreElements()) {
			System.out.println( st.nextElement() );
		}
	}
	
//	OPERAND TEST
	private static void operandTest() {
		
		System.out.println( StringUtils.center("Operand Test", PAD * 2));
		for (int pos = 0; pos < (PAD*2); pos++) System.out.print('=');
		System.out.println();
		System.out.println(
				StringUtils.leftPad("Input", PAD) + 
				StringUtils.leftPad("Result", PAD)
				);
		for ( String operandStr : TEST_OPERANDS ) {
			OperandNumeric operand = new OperandNumeric( operandStr );
			System.out.println(
					StringUtils.leftPad(operandStr + ":", PAD) +
					StringUtils.leftPad(Double.toString(operand.evaluate()),
							PAD) );
		}
	}
	
//	OPERATOR TEST
	private static void operatorTest() {
		Operator o;
		OperandNumeric left, right;
		
		System.out.println( StringUtils.center("Operand Test", 60));
		for ( int pos = 0; pos++ < 60; System.out.print('='));
		System.out.println();
		
		leftPadPrint10("Left");
		leftPadPrint10("Right");
		leftPadPrint10("Add");
		leftPadPrint10("Subtract");
		leftPadPrint10("Multiply");
		leftPadPrintln10("Divide");
		System.out.println();
		
		OperandNumeric[][] onPairs = operandPairs();
		for ( OperandNumeric[] onPair : onPairs ) {
					left = onPair[0];
					right = onPair[1];
					
					System.out.print(
					StringUtils.leftPad(left.toString(), PAD) +
					StringUtils.leftPad(right.toString(), PAD)
					);
			
			o = new OperatorAdd(left, right);
			leftPadPrint10( Double.toString( o.evaluate() ) );
			
			o = new OperatorSubtract(left, right);
			leftPadPrint10( Double.toString (o.evaluate() ) );
			
			o = new OperatorMultiply(left, right);
			leftPadPrint10( Double.toString( o.evaluate() ) );
			
			try {
			o = new OperatorDivide(left, right);
			leftPadPrintln10( Double.toString( o.evaluate() ) );
			} catch (IllegalArgumentException e) {
				leftPadPrintln10("*NaN*");
			}
		}
		
	}

	private static void divideByZeroTest() {
		
	}
	
	private static void centerPadPrintln20( String str ) {
		System.out.println( StringUtils.center(str, 20));
	}
	
	private static void leftPadPrint10( String str ) {
		System.out.print(StringUtils.leftPad(str, PAD));
	}
	
	private static void leftPadPrintln10( String str ) {
		System.out.println(StringUtils.leftPad(str, PAD));
	}
	
	private static OperandNumeric[][] operandPairs() {
		int len = TEST_OPERANDS.length / 2;
		OperandNumeric[][] onPairs = new OperandNumeric[len][2];
		
		int pairPos = 0;
		int opPos = 0;
		while ( pairPos < onPairs.length && opPos < TEST_OPERANDS.length ) {
			onPairs[pairPos][0] = new OperandNumeric(TEST_OPERANDS[opPos]);
			onPairs[pairPos++][1] = new OperandNumeric(TEST_OPERANDS[++opPos]);
		}
		
		return onPairs;
	}

}
