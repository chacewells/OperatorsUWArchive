package edu.uweo.javaintro.homework.operators;

import org.apache.commons.lang3.StringUtils;


public abstract class Operand {
	private static final String REGEX = "[-\\+]";
	private String operand;
	int sign;
	
	public abstract double evaluate();

	public Operand( String operand ) {
		parseSignApache( operand );
		parseOperand( operand );
	}
	
	public int getSign() {
		return sign;
	}
	
//	sign parser using Apache Commons library
//	one liner methods... it's a beautiful thing
	private void parseSignApache( String operand ) {
		sign = ( ( StringUtils.countMatches(operand, "-") % 2 ) == 0 ) ? 1 : -1;
	}
	
	private void parseOperand( String operand ) {
		this.operand = operand.replaceAll(REGEX, "");
	}

	public String getOperand() {
		return operand;
	}
	
	@Override
	public String toString() {
		return ( new StringBuilder() )
				.append( ( getSign() < 0 ) ? "-" : "")
				.append( operand )
				.toString();
	}
	
//	parses sign by comparing individual chars
	
//	private void parseSignThreadsafe( String operand ) {
//		int position;
//		for ( position = 0, sign = 1
//				;
//				position < operand.length()
//				&& (operand.charAt(position) == '-' 
//					|| operand.charAt(position) == '+')
//				;
//				sign *= ( operand.charAt(position++) == '-' ) ? -1 : 1
//						);
//	}
	
//	alternate sign parser method: apparently not threadsafe
	
//	private void parseSign( String operand ) {
//		int position;
//		for ( position = 0, sign = 1;
//				position < operand.length()
//				&& operand.substring(position, position + 1).matches(REGEX)
//				;
//				sign *= ( operand.charAt(position++) == '-' ) ? -1 : 1);
//	}

}
